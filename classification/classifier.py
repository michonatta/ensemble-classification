from sklearn.model_selection import cross_validate, cross_val_predict, StratifiedKFold
from sklearn import metrics


class Classifier:
    def __init__(self, classifier, data, target):
        self.output = []
        self.classifier = classifier
        self.data = data
        self.target = target
        self.tpe = 0
        self.tne = 0
        self.fpe = 0
        self.fne = 0
        self.tpc = 0
        self.tnc = 0
        self.fpc = 0
        self.fnc = 0
        # self.tpo = 0
        # self.tno = 0
        # self.fpo = 0
        # self.fno = 0
        # self.eco = 0
        # self.oce = 0
        self.cce = 0
        # self.cco = 0
        # self.occ = 0
        self.ecc = 0

    def cross_validate(self):
        scores = cross_val_predict(self.classifier, self.data, self.target, cv=StratifiedKFold(10))

        matrix = metrics.confusion_matrix(self.target, scores)
        if matrix[0][0] >= 480:
            print(matrix)

        self.set_metrics(scores)
        print(scores)
        report = metrics.classification_report(self.target, scores, digits=4)
        print(report)


        # print(self.sds())
        # prec, rec, f, support = metrics.precision_recall_fscore_support(scores, self.target, average="micro")
    def predict(self):
        scores = self.classifier.predict(self.data)
        print(scores)
        self.set_metrics(scores)
        report = metrics.classification_report(self.target, scores, digits=4)
        print(metrics.confusion_matrix(self.target, scores))
        print(report)
        return metrics.confusion_matrix(self.target, scores)

    def set_metrics(self, scores):
        confusion_matrix = metrics.confusion_matrix(self.target, scores)
        self.elongated_metrics(confusion_matrix)
        self.circular_metrics(confusion_matrix)
        # if confusion_matrix.shape[0] == 3:
        #     self.others_metrics(confusion_matrix)

        elongate_acc = self.accuracy(self.tpe, self.tne, self.fpe, self.fne)
        circular_acc = self.accuracy(self.tpc, self.tnc, self.fpc, self.fnc)
        # if confusion_matrix.shape[0] == 3:
        #     other_acc = self.accuracy(self.tpo, self.tno, self.fpo, self.fno)
        average_acc = self.average(circular_acc, elongate_acc)

        elongate_sp = self.specificity(self.tne, self.fpe)
        circular_sp = self.specificity(self.tnc, self.fpc)
        # if confusion_matrix.shape[0] == 3:
        #     others_sp = self.specificity(self.tno, self.fpo)
        average_sp = self.average(circular_sp, elongate_sp)

        print("circular sp: " + str(circular_sp))
        print("elongated sp: " + str(elongate_sp))
        # if confusion_matrix.shape[0] == 3:
        #     print("other sp: " + str(others_sp))
        print("average sp: " + str(average_sp))

        print("circular acc: " + str(circular_acc))
        print("elongated acc: " + str(elongate_acc))
        # if confusion_matrix.shape[0] == 3:
        #     print("other acc: " + str(other_acc))
        print("average acc: " + str(average_acc))

    def elongated_metrics(self, confusion_matrix):
        self.tpe = confusion_matrix[1][1]
        self.fpe = confusion_matrix[0][1]
        self.fne = confusion_matrix[1][0]
        self.tne = confusion_matrix[0][0]

        if confusion_matrix.shape[0] == 3:
            self.fpe += confusion_matrix[2][1]
            self.fne += confusion_matrix[1][2]
            self.tne += confusion_matrix[0][2] + confusion_matrix[2][0] + confusion_matrix[2][2]
            self.eco = confusion_matrix[1][2]
            self.ecc = confusion_matrix[1][0]

    def circular_metrics(self, confusion_matrix):
        self.tpc = confusion_matrix[0][0]
        self.fpc = confusion_matrix[1][0]
        self.fnc = confusion_matrix[0][1]
        self.tnc = confusion_matrix[1][1]

        if confusion_matrix.shape[0] == 3:
            self.fpc += confusion_matrix[2][0]
            self.fnc += confusion_matrix[0][2]
            self.tnc += confusion_matrix[1][2] + confusion_matrix[2][1] + confusion_matrix[2][2]
            self.cce = confusion_matrix[0][1]
            self.cco = confusion_matrix[0][2]

    def others_metrics(self, confusion_matrix):
        if confusion_matrix.shape[0] == 3:
            self.tpo = confusion_matrix[2][2]
            self.fpo = confusion_matrix[0][2] + confusion_matrix[1][2]
            self.fno = confusion_matrix[2][0] + confusion_matrix[2][1]
            self.tno = confusion_matrix[0][0] + confusion_matrix[0][1] + confusion_matrix[1][0] + confusion_matrix[1][1]
            self.oce = confusion_matrix[2][1]
            self.occ = confusion_matrix[2][0]

    def precision(self, tp, fp):
        return round(tp/(tp + fp), 4)

    def recall(self, tp, fn):
        return round(tp/(tp + fn), 4)

    def f1(self, prec, rec):
        return round(2*prec*rec/(prec + rec), 4)

    def specificity(self, tn, fp):
        return round(tn/(tn + fp), 4)

    def accuracy(self, tp, tn, fp, fn):
        return round((tp + tn)/(tp + fp + fn + tn), 4)

    def average(self, circular, elongated, others=0):
        return round((circular + elongated + others)/3, 4)

    def sds(self):
        return round((self.tpc + self.tpe + self.tpo + self.eco + self.oce) / (self.tpc + self.tpe + self.tpo + self.cce
                     + self.cco + self.occ + self.ecc + self.eco + self.oce), 4)

    @staticmethod
    def fne(clf, data, target):
        scores = cross_val_predict(clf, data, target, cv=10)
        confusion_matrix = metrics.confusion_matrix(target, scores)
        tpe = confusion_matrix[1][1]
        fpe = confusion_matrix[0][1]
        fne = confusion_matrix[1][0]
        tne = confusion_matrix[0][0]
        if confusion_matrix.shape[0] == 3:
            fpe += confusion_matrix[2][1]
            fne += confusion_matrix[1][2]
            tne += confusion_matrix[0][2] + confusion_matrix[2][0] + confusion_matrix[2][2]

        return round((tpe)/(tpe + fne), 4)

    @staticmethod
    def true_pos(clf, data, target):
        scores = cross_val_predict(clf, data, target, cv=10)
        confusion_matrix = metrics.confusion_matrix(target, scores)
        tpe = confusion_matrix[1][1]
        return tpe

    @staticmethod
    def elongated_acc(clf, data, target):
        scores = cross_val_predict(clf, data, target, cv=10)
        confusion_matrix = metrics.confusion_matrix(target, scores)
        tpe = confusion_matrix[1][1]
        fpe = confusion_matrix[0][1]
        fne = confusion_matrix[1][0]
        tne = confusion_matrix[0][0]
        if confusion_matrix.shape[0] == 3:
            fpe += confusion_matrix[2][1]
            fne += confusion_matrix[1][2]
            tne += confusion_matrix[0][2] + confusion_matrix[2][0] + confusion_matrix[2][2]

        return round((tpe + tne)/(tpe + fpe + fne + tne), 4)

    @staticmethod
    def sds_func(clf, data, target):
        scores = cross_val_predict(clf, data, target, cv=10)
        confusion_matrix = metrics.confusion_matrix(target, scores)
        tpe = confusion_matrix[1][1]
        ecc = confusion_matrix[1][0]
        tpc = confusion_matrix[0][0]
        cce = confusion_matrix[0][1]
        eco_oce = 0
        tpo = confusion_matrix[2][2]

        if confusion_matrix.shape[0] == 3:
            eco = confusion_matrix[1][2]
            cco = confusion_matrix[0][2]
            occ = confusion_matrix[2][0]
            oce = confusion_matrix[2][1]
            cce += cco
            eco_oce = eco + oce
            ecc += occ

        metric = round((tpc + tpe + tpo + eco_oce) / (tpc + tpe + tpo + cce+ ecc + eco_oce), 4)
        print(metric)
        return metric

    @staticmethod
    def sds_scorer(target, scores):
        confusion_matrix = metrics.confusion_matrix(target, scores)
        tpe = confusion_matrix[1][1]
        ecc = confusion_matrix[1][0]
        tpc = confusion_matrix[0][0]
        cce = confusion_matrix[0][1]
        eco_oce = 0
        tpo = 0

        if confusion_matrix.shape[0] == 3:
            tpo = confusion_matrix[2][2]
            eco = confusion_matrix[1][2]
            cco = confusion_matrix[0][2]
            occ = confusion_matrix[2][0]
            oce = confusion_matrix[2][1]
            cce += cco
            eco_oce = eco + oce
            ecc += occ

        metric = round((tpc + tpe + tpo + eco_oce) / (tpc + tpe + tpo + cce + ecc + eco_oce), 4)
        print(metric)
        return metric