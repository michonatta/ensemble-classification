def form_factoring(circularity):
    res = []
    for cell in circularity:
        if 0.5 < cell[0] <= 1:
            res.append('c')
        else:
            res.append('e')

    return res


def asakura(cells):
    res = []

    for cell in cells:

        csf = cell[0]
        esf = cell[1]

        if esf < 0.5:
            res.append('e')
        elif csf < 0.8:
            res.append('o')
        else:
            res.append('c')

    return res


def gonzalez_hidalgo(esf):
    res = []

    for cell in esf:
        if cell[0] <= 0.6:
            res.append('e')
        else:
            res.append('c')

    return res