from sklearn import metrics


def sds_func(predicted=None, target=None, confusion_matrix=None):
    if confusion_matrix is None:
        confusion_matrix = metrics.confusion_matrix(target, predicted)
    tpe = confusion_matrix[1][1]
    ecc = confusion_matrix[1][0]
    tpc = confusion_matrix[0][0]
    cce = confusion_matrix[0][1]

    # if confusion_matrix.shape[0] == 3:
    #     eco = confusion_matrix[1][2]
    #     oce = confusion_matrix[2][1]
    #     occ = confusion_matrix[2][0]
    #     tpo = confusion_matrix[2][2]
    #     cco = confusion_matrix[0][2]
    # else:
    eco, oce, occ, tpo, cco = 0, 0, 0, 0, 0
    metric = round((tpc + tpe + tpo + eco + oce) / (tpc + tpe + tpo + cce + cco + occ + ecc + eco + oce), 4)
    return metric


def elongated_metrics(confusion_matrix=None):
    tpe = confusion_matrix[1][1]
    fpe = confusion_matrix[0][1]
    fne = confusion_matrix[1][0]
    tne = confusion_matrix[0][0]
    # eco = confusion_matrix[1][2]
    # ecc = confusion_matrix[1][0]
    if confusion_matrix.shape[0] == 3:
        fpe += confusion_matrix[2][1]
        fne += confusion_matrix[1][2]
        tne += confusion_matrix[0][2] + confusion_matrix[2][0] + confusion_matrix[2][2]

    prec = precision(tpe, fpe)
    rec = recall(tpe, fne)
    return accuracy(tpe, tne, fpe, fne), specificity(tne, fpe), prec, rec, f1(prec,rec)


def circular_metrics(confusion_matrix=None):
     tpc = confusion_matrix[0][0]
     fpc = confusion_matrix[1][0]
     fnc = confusion_matrix[0][1]
     tnc = confusion_matrix[1][1]
     # cce = confusion_matrix[0][1]
     # cco = confusion_matrix[0][2]
     if confusion_matrix.shape[0] == 3:
         fpc += confusion_matrix[2][0]
         fnc += confusion_matrix[0][2]
         tnc += confusion_matrix[1][2] + confusion_matrix[2][1] + confusion_matrix[2][2]

     prec = precision(tpc, fpc)
     rec = recall(tpc, fnc)
     return accuracy(tpc, tnc, fpc, fnc), specificity(tnc, fpc), prec, rec, f1(prec, rec)


def others_metrics(confusion_matrix):

    tpo = confusion_matrix[2][2]
    fpo = confusion_matrix[0][2] + confusion_matrix[1][2]
    fno = confusion_matrix[2][0] + confusion_matrix[2][1]
    tno = confusion_matrix[0][0] + confusion_matrix[0][1] + confusion_matrix[1][0] + confusion_matrix[1][1]
    # oce = confusion_matrix[2][1]
    # occ = confusion_matrix[2][0]

    return accuracy(tpo, tno, fpo, fno), specificity(tno, fpo)


def specificity(tn, fp):
    return round(tn/(tn + fp), 4)


def accuracy(tp, tn, fp, fn):
    return round((tp + tn)/(tp + fp + fn + tn), 4)


def precision(tp, fp):
    return round(tp/(tp + fp), 4)


def recall(tp, fn):
    return round(tp/(tp + fn), 4)


def f1(prec, rec):
    return round(2*prec*rec/(prec + rec), 4)
