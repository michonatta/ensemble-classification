from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn import tree
import sklearn.svm as svm
from classification import metrics as m
from sklearn import metrics
import classification.classifier as clsf
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.feature_selection import SelectKBest, mutual_info_classif
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import pandas as pd
from sklearn import preprocessing as pp
from sklearn.model_selection import train_test_split
import time
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import VotingClassifier
import itertools
from sklearn.ensemble import StackingClassifier


df = pd.concat([pd.read_csv('data_processing/new_features/shape.csv'),
                pd.read_csv('data_processing/new_features/texture.csv'),
                pd.read_csv('data_processing/new_features/color.csv')], axis=1, sort=False)

X_train, X_test, y_train, y_test = train_test_split(df.loc[:], df['Class'], test_size=0.3,
                                                            random_state=1, stratify=df['Class'])

svc = svm.SVC(C=10, kernel='rbf', gamma=0.1, probability=True)
gb = GradientBoostingClassifier(random_state=1, n_estimators=50, min_samples_split=20, min_samples_leaf=5,
                                max_features='log2', max_depth=30, learning_rate=0.05)
rf = RandomForestClassifier(n_estimators=80, min_samples_split=10, max_features='log2', max_depth=None,
                            min_samples_leaf=2, bootstrap=True, random_state=1)

et = ExtraTreesClassifier(random_state=1, n_estimators=30, min_samples_split=10, min_samples_leaf=1,
                          max_features='auto',
                          max_depth=20, bootstrap=True)
dt = tree.DecisionTreeClassifier(min_samples_leaf=5, max_depth=10, splitter='random', min_samples_split=20,
                                 random_state=1, max_features=None)
knn = KNeighborsClassifier(n_neighbors=7, weights='uniform', p=3, algorithm='auto')
mlp = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(10, 3), activation='tanh', random_state=1,
                    max_iter=2000)
log_reg = LogisticRegression(class_weight='balanced', multi_class='multinomial', penalty='elasticnet', solver='saga',
                             l1_ratio=0.3)
scaler = pp.StandardScaler()
X_train = scaler.fit_transform(X_train.loc[:, X_train.columns != 'Class'])
X_test = scaler.fit_transform(X_test.loc[:, X_test.columns != 'Class'])

models = [('et', et), ('dt', dt), ('svm', svc), ('mlp', mlp), ('rf', rf), ('gb', gb), ('knn', knn)]

for L in range(2, len(models)+1):
    for subset in itertools.combinations(models, L):
        print('------------')
        print(subset)
        print('------------')
        stacking = StackingClassifier(estimators=subset, final_estimator=tree.DecisionTreeClassifier())
        stacking.fit(X_train, y_train)
        scores = stacking.predict(X_test)

        print(metrics.confusion_matrix(y_test, scores))
        print(m.sds_func(scores, y_test))
        print(metrics.classification_report(y_true=y_test, y_pred=scores, digits=4))

        voting = VotingClassifier(estimators=subset)
        voting.fit(X_train, y_train)
        scores = voting.predict(X_test)
        print(metrics.confusion_matrix(y_test, scores))
        print(m.sds_func(scores, y_test))
        print(metrics.classification_report(y_true=y_test, y_pred=scores, digits=4))

